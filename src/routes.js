import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from './pages/Home';
import Estabelecimentos from './pages/Estabelecimentos';

export default function Routes(){
   return (
      <Switch>
         <Route path="/" exact component={Home} />
         <Route path="/estabelecimentos" exact component={Estabelecimentos} />
      </Switch>
   );
}