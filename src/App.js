import React from "react";
import { BrowserRouter } from "react-router-dom";
import Routes from "./routes";
import { Provider } from "react-redux";

//reactotron
import "./config/reactotronConfig";

import store from "./store/store";

import Header from "./components/Header";
import Aside from "./components/Aside";

import "./App.scss";

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className="App">
          <div className="container-fluid">
            <div className="row">
              <Header />
              <Aside />

              <main class="col-12 col-sm-9">
                <Routes />
              </main>
            </div>
          </div>
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
