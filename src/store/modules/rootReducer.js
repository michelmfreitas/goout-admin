import { combineReducers } from 'redux';

import estabelecimentos from './estabelecimentos/reducer';

export default combineReducers({
   estabelecimentos,
});