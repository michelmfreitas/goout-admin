import React from 'react';

import logo from '../images/logo_goout.svg';

export default function Header() {
  return (
   <header class="col-12">
      <figure>
         <img src={logo} alt="Go Out" title="Go Out" />
      </figure>
      <nav>
         <a href="/" title="Sair">Sair</a>
      </nav>
   </header>
  );
}
