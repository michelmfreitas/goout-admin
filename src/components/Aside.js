import React from 'react';

export default function Aside() {
  return (
   <aside class="col-12 col-sm-3 col-lg-2">
      <nav>
         <a href="/estabelecimentos" title="">Estabelecimentos</a>
         <a href="/" title="">Anúncios</a>
         <a href="/" title="">Eventos</a>
      </nav>
   </aside>
  );
}
